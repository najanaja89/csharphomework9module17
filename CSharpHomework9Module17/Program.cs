﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace CSharpHomework9Module17
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            string menu = "";

            while (true)
            {
                Console.WriteLine("Press 1 to start task 1");
                Console.WriteLine("Press 2 to start task 2");
                menu = Console.ReadLine();

                switch (menu)
                {
                    case "1":
                        string xmlPath = "https://habr.com/rss/interesting/";
                        XmlDocument xmlDocument = new XmlDocument();
                        xmlDocument.Load(xmlPath);
                        XmlElement rootElement = xmlDocument.DocumentElement;

                        List<Item> items = new List<Item>
                {
                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[9].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[9].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[9].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[9].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[10].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[10].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[10].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[10].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[11].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[11].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[11].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[11].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[12].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[12].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[12].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[12].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[13].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[13].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[13].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[13].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[14].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[14].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[14].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[14].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[15].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[15].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[15].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[15].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[16].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[16].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[16].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[16].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[17].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[17].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[17].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[17].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[18].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[18].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[18].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[18].ChildNodes[4].LastChild.Value
                },

                new Item()
                {
                    Title = rootElement.ChildNodes[0].ChildNodes[19].ChildNodes[0].LastChild.Value,
                    Link = rootElement.ChildNodes[0].ChildNodes[19].ChildNodes[2].LastChild.Value,
                    Description = rootElement.ChildNodes[0].ChildNodes[19].ChildNodes[3].LastChild.Value,
                    PubDate = rootElement.ChildNodes[0].ChildNodes[19].ChildNodes[4].LastChild.Value
                },

            };
                        foreach (var item in items)
                        {
                            Console.WriteLine("-------------------------------------------");
                            Console.WriteLine(item.Title);
                            Console.WriteLine();
                            Console.WriteLine(item.Link);
                            Console.WriteLine();
                            Console.WriteLine(item.Description);
                            Console.WriteLine();
                            Console.WriteLine(item.PubDate);
                            Console.WriteLine("-------------------------------------------");
                            Console.WriteLine();
                        }

                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "2":
                        XmlDocument student = new XmlDocument();
                        using (StreamReader streamReader = new StreamReader("student.xml"))
                        {
                            student.LoadXml(streamReader.ReadToEnd());
                        }

                        XmlElement rootStudent = student.DocumentElement;

                        Console.WriteLine();
                        Console.WriteLine(rootStudent.ChildNodes[0].Name + "\t" + rootStudent.ChildNodes[0].LastChild.Value);
                        Console.WriteLine(rootStudent.ChildNodes[1].Name + "\t" + rootStudent.ChildNodes[1].LastChild.Value);
                        Console.WriteLine(rootStudent.ChildNodes[2].Name + "\t" + rootStudent.ChildNodes[2].LastChild.Value);
                        Console.WriteLine(rootStudent.ChildNodes[3].Name + "\t" + rootStudent.ChildNodes[3].LastChild.Value);

                        Console.ReadLine();
                        Console.Clear();
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
